# HSPC Reference Platform Installer #

Welcome to the HSPC Reference Platform Installer!

## Destination Environment

Systems will be installed in the target environment by the HSPC Installer (localhost, vm, hspc-test, hspc-prod, etc) follows

### Host Machine Setup

````
sudo apt-get update && \
sudo apt-get -y install curl git python-pycurl python-pip python-yaml python-paramiko python-jinja2 && \
sudo pip install ansible==2.3.2.0 && \
sudo pip install --upgrade pip
````

### Remote Install on Ubuntu 16.04

In this install, we are going to build a SMART on FHIR platform on a remove Ubuntu 16.04 server.

#### Prepare the remote server

From the remote server:

````
sudo apt-get update
sudo apt-get -y install curl git python-pycurl python-pip python-yaml python-paramiko python-jinja2
sudo pip install --upgrade pip
sudo pip install ansible==2.3.2.0
````

If custom certificates are going to be used, upload them to the server.

#### Prepare the installer

````
git clone https://bitbucket.org/hspconsortium/installer.git
cd installer
ansible-galaxy install -r roles/requirements.yml -p ./roles/ --force
````

Modify the environment properties:
```
vi environments/local.yml
vi environments/test.yml
vi environments/prod.yml
```

The following properties will need to be passed to the installer.  We pass these as variables in the Ansible Tower inventory:

| Property                                  | Value      | Notes                                                    |
| ----------------------------------------- | ---------- | -------------------------------------------------------- |
| env                                       | "test"     | Should align with the environment filename you are using |
| hosting_userpass                          | ""         | Password of the hosting user                             |
| keystore_password                         | "changeme" |                                                          |
| hspc_platform_jwt_key                     | "changeme" |                                                          |
| aws_access_key_id                         | "changeme" |                                                          |
| aws_secret_access_key                     | "changeme" |                                                          |
| aws_ec2_volume_id                         | "changeme" |                                                          |
| mysql_username                            | "hspc"     |                                                          |
| mysql_password                            | "password" |                                                          |
| firebase_apiKey                           | "changeme" |                                                          |
| firebase_messagingSenderId                | "changeme" |                                                          |
| account_api_newsletterId1                 | "changeme" |                                                          |
| account_api_newsletterId2                 | "changeme" |                                                          |
| account_api_mailchimpApiKey               | "changeme" |                                                          |
| auth_server_admin_password                | "changeme" |                                                          |
| messaging_mail_server_username            | "changeme" |                                                          |
| messaging_mail_server_password            | "changeme" |                                                          |
| sandbox_server_admin_access_client_secret | "changeme" |                                                          |

#### Run the Installer

Make a "local" connection using the "localhost" inventory file:

````
sudo ansible-playbook -i inventory/hosts site.yml
````

### Port Assignments
| System          | Internal Port | External Port |
| --------------- | -------------:| -------------:|
| MySQL           |          3306 |          3306 |
| Auth            |          8060 |          9060 |
| Account         |          8065 |          9065 |
| Account API     |          8066 |          9066 |
| API DSTU2       |          8071 |          9071 |
| API STU3 V2     |          8072 |          9072 |
| API STU3 V3     |          8073 |          9073 |
| API STU3        |          8074 |          9074 |
| Sandbox Mgr API |         12000 |         13000 |
| Sandbox Mgr     |          8080 |          9080 |
| BP Centiles     |          8081 |          9081 |
| Cardiac Risk    |          8082 |          9082 |
| Gallery         |          8085 |          9085 |
| Bili Risk Cht   |          8086 |          9086 |
| Bili Monitor    |          8087 |          9087 |
| Bili CDS Hks    |          8088 |          9088 |
| Growth Chart    |          8089 |          9089 |
| Messaging       |          8091 |          9091 |
| PWM             |          8092 |          9092 |
| Apps            |          8093 |          9093 |
| Patient Pckr    |          8094 |          9094 |
| Appointments    |          8095 |          9095 |
| Pat Data Mgr    |          8096 |          9096 |

## Windows Notes
* Windows Note: Ansible is not supported on Windows. If you want to build a SMART on FHIR VM on Windows, you should create a VM instance for Ubuntu 16.04 and perform a please use the version of the installer which runs Ansible on the guest machine instead of using the one on the host OS. To enable this mode, please edit Vagrantfile by commenting out the "ansible" provisioner and enabling second "shell" provisioner before running vagrant up. An alternative options is to follow the instructions in the "Building SMART-on-FHIR on fresh Ubuntu 16.04 machine (without Vagrant)" section in this document.

*Windows Note: The default installation of GIT on Windows enables a LF to CRLF conversion upon checkout which is going to mess up the install. You will need to make sure that this conversion is disabled by running:

    git config --global core.autocrlf false
